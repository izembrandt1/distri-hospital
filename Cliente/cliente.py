import websockets
import asyncio
import json
import logging

#Configuracion de logging para el cliente
logging.basicConfig(filename='respuesta_servidor.log',
                    format='%(asctime)s;%(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S',
                    filemode='w',
                    level=logging.INFO
                    )

def gestionar_impresion(respuesta):
    #Se separan las variables
    informacion = respuesta.get('informacion')
    estado = respuesta.get('estado')
    hospitales = respuesta.get('hospitales')
    operacion = respuesta.get('operacion')
    hospital_id = respuesta.get('hospital_id')
    cama_id = respuesta.get('cama_id')
    #Se procesa los datos recibidos para crear una respuesta
    if estado == 0: #Se accede si la respuesta del servidor resulto exitosa
        if operacion == "ver_hospitales_con_camas":
            ver_hospitales_con_camas(hospitales)
        elif operacion == "ver_camas":
            ver_camas(hospitales,hospital_id)                          
        elif operacion == "ver_hospitales":
            ver_hospitales(hospitales)
        elif operacion == "crear_cama":
            ultimo_id = len(hospitales[hospital_id]["camas"]) #Obtenemos el id de la nueva cama creada (la ultima cama)
            msj_accion_cama(operacion,hospital_id,ultimo_id)
        elif operacion == "eliminar_cama" or operacion == "ocupar_cama" or operacion == "desocupar_cama": 
            msj_accion_cama(operacion,hospital_id,cama_id)
        else:
            print("si ves este mensaje, enviar un mensaje a los desarrolladores")
    elif estado == 2: #Se accede si la respuesta del servidor es incorrecta por no existir la cama 
        print("Lo sentimos, la cama " + str(cama_id) + " no existe en el hospital " + hospital_id) #Se imprime el mensaje
    elif estado == 4: #Se accede si la respuesta del servidor es incorrecta por no existir el hospital
        print("Lo sentimos, el hospital con id " + hospital_id  +" no existe. Intente de nuevo...") #Se imprime el mensaje
    elif estado == 6: # Se accede si la respuesta del servidor es incorrecta por que la cama esta ocupada
         print("Lo sentimos, la cama " + str(cama_id) + " en el hospital " + hospital_id  +" esta ocupada actualmente!")
    elif estado == 8: # Se accede si la respuesta del servidor es incorrecta por que la cama esta desocupada
         print("La cama " + str(cama_id) + " en el hospital " + hospital_id  +" ya esta desocupada!")
    elif estado == 10: # Se accede si la respuesta del servidor es incorrecta por que el valor del id no es numerico
        if operacion == "ver_camas" or operacion == "crear_cama": #Cuando solo se envia el id del hospital
            print("Lo sentimos, el id '" + hospital_id  +"' no es numerico o valido. Intente de nuevo...")
        elif operacion == "eliminar_cama" or operacion == "ocupar_cama" or operacion == "desocupar_cama": #Cuando se envia el id del hospital y de la cama
            print("Lo sentimos, el id de la cama '" + str(cama_id) + "' o/y el id del hospital '" + hospital_id  +"' no es numerico o valido. Intente de nuevo...")  
    elif estado == -1: # Se accede si la respuesta del servidor es incorrecta por algun error en el mismo
        print("Lo sentimos, operacion fallida. Intente mas tarde...")

def ver_hospitales(hospitales):
    print("\tID\tHOSPITAL")
    for hospital in hospitales:
        print("\t" + hospital + "\t" + hospitales[hospital]["nombre"])#Despliega el nombre del hospital y su codigo

def ver_camas(hospitales,hospital_id):
    print("En el hospital " + hospitales[hospital_id]["nombre"] + " con codigo= " + hospital_id + ":") #Despliega el nombre del hospital y su codigo
    for cama in hospitales[hospital_id]["camas"]: #Recorre la lista de camas del hospital seleccionado
        print("\tLa cama "+ str(cama["id"]) + " esta "+ ("ocupado" if cama["ocupado"] else "libre")) #Despliega la lista de las camas del hospital seleccionado
    if len(hospitales[hospital_id]["camas"]) == 0:
        print("\tNo hay camas registradas")
    print("\n")  

def ver_hospitales_con_camas(hospitales):
    for hospital in hospitales:
        print("En el hospital " + hospitales[hospital]["nombre"] + " con codigo= "+ hospital +":") #Despliega el nombre del hospital y su codigo
        for cama in hospitales[hospital]["camas"]:
            print("\tLa cama "+ str(cama["id"]) + " esta "+ ("ocupado" if cama["ocupado"] else "libre")) #Despliega la lista de las camas del hospital
        if len(hospitales[hospital]["camas"]) == 0:
            print("\tNo hay camas registradas") #Despliega un mensaje que informa que no hay camas en el hospital
        print("\n")

def msj_accion_cama(operacion,hospital_id,cama_id):
    operacionSplit = operacion.split("_")
    print("Se logro " + operacionSplit[0] + " la cama " + str(cama_id) + " en el hospital " + hospital_id) #Se imprime la confirmacion

def imprimir_menu():
    print("\nIngrese una de las siguientes opciones:")  
    print("1: Consultar Hospitales.")
    print("2: Consultar Camas de un Hospital.")
    print("3: Consultar todos los Hospitales y sus Camas.")
    print("4: Agregar Cama.")
    print("5: Eliminar Cama.")
    print("6: Ocupar Cama.")
    print("7: Desocupar Cama.")
    print("0: Salir del Programa.")

def leer_opcion():
    #se leen la entrada del usuario
    opcion = str(input())
    #Se llaman a las variables globales donde se esperan guardar los id del hospital y de la cama que se envian al servidor
    global hospital_id 
    global cama_id
    #se limpian las variables globales
    hospital_id = ""
    cama_id = ""
    #se obtienen los datos extra de acuerdo a la opcion
    if opcion == "1":
        operacion = "ver_hospitales"
    elif opcion == "2":      
        operacion = "ver_camas"
        hospital_id = str(input("Ingrese el codigo del hospital\n"))
    elif opcion == "3":
        operacion = "ver_hospitales_con_camas"
    elif opcion == "4":
        operacion = "crear_cama"      
        hospital_id = str(input("Ingrese el codigo del hospital\n"))       
    elif opcion == "5":
        operacion = "eliminar_cama"
        hospital_id = str(input("Ingrese el codigo del hospital\n"))
        cama_id = str(input("Ingrese el codigo de la cama\n"))      
    elif opcion == "6":
        operacion = "ocupar_cama"
        hospital_id = str(input("Ingrese el codigo del hospital\n"))
        cama_id = str(input("Ingrese el codigo de la cama\n"))
    elif opcion == "7":
        operacion = "desocupar_cama"
        hospital_id = str(input("Ingrese el codigo del hospital\n"))
        cama_id = str(input("Ingrese el codigo de la cama\n")) 
    elif opcion == "0":
        return "salir"
    else:
        print("Ingrese solamente los numeros del '0' al '7'")
        return "error"
    #se serializa los datos para poder enviar al servidor
    return json.dumps({"operacion": operacion, "hospital_id": hospital_id, "cama_id":cama_id})

#Funcion prinicipal para recibir y enviar mensajes al servidor
async def listen():
    url = "ws://127.0.0.1:7890"
    # Conectar al servidor
    async with websockets.connect(url) as ws:
        print("Bienvenido al sistema de Hospitales Distribuidos")
        # Permanecer ejecutandose para siempre, o hasta que el usuario quiera
        while True:
            imprimir_menu()
            mensaje = leer_opcion()
            #se verifica que el usuario quizo realizar una operacion
            if mensaje == "salir":
                break
            elif mensaje != "error":
                #se serializa para enviar al servidor
                mensaje_enviado = mensaje.encode()
                #se envia el mensaje
                await ws.send(mensaje_enviado)
                #Esperar respuesta
                respuesta_recibida = await ws.recv()
                #Se procesa la respuesta
                respuesta = json.loads(respuesta_recibida)
                #Se hace el log de la respuesta
                logging.info(respuesta)
                #Se imprime la respuesta recibida
                gestionar_impresion(respuesta)
            print("\tPresione ENTER para seguir..")
            input()

#Comenzar la conexion
asyncio.get_event_loop().run_until_complete(listen())


  