# distri-hospital
## Proyecto para la materia Sistemas Distribuidos


## 1. Los nombres de los alumnos que realizaron el trabajo.
   - ##### Ruben Izembrandt
   - ##### Jose Medina
   - ##### Pablo Perez
   - ##### Celeste Mallorquin

## 2. Documento de resumen de investigación de WebSocket.
**WebSocket** 

Es un protocolo de comunicaciones informáticas que proporciona canales de comunicación full-duplex a través de una única conexión TCP.     
El protocolo WebSocket fue estandarizado por el IETF como RFC 6455 en 2011, y la API de WebSocket en Web IDL está siendo estandarizada por el W3C.

WebSocket es distinto de HTTP. Ambos protocolos están ubicados en la capa 7 en el modelo OSI y dependen de TCP en la capa 4. Aunque son          diferentes, RFC 6455 establece que WebSocket "está diseñado para funcionar en los puertos HTTP 443 y 80, así como para admitir intermediarios y proxies HTTP. " haciéndolo compatible con HTTP. Para lograr la compatibilidad, el protocolo de enlace de WebSocket utiliza el encabezado de actualización HTTP para cambiar del protocolo HTTP al protocolo WebSocket.

El protocolo WebSocket permite la interacción entre un navegador web (u otra aplicación cliente) y un servidor web con una sobrecarga más baja que las alternativas half-duplex como el sondeo HTTP, lo que facilita la transferencia de datos en tiempo real desde y hacia el servidor. Esto es posible al proporcionar una forma estandarizada para que el servidor envíe contenido al cliente sin que el cliente lo solicite primero, y al permitir que los mensajes se pasen de un lado a otro mientras se mantiene la conexión abierta. De esta manera, puede tener lugar una conversación continua bidireccional entre el cliente y el servidor. Las comunicaciones generalmente se realizan a través del puerto TCP número 443 (u 80 en el caso de conexiones no seguras), lo cual es beneficioso para entornos que bloquean las conexiones a Internet que no son de la web usando un cortafuegos. Se han logrado comunicaciones similares de dos vías navegador-servidor de formas no estandarizadas utilizando tecnologías provisionales como Comet o Adobe Flash Player.

La mayoría de los navegadores admiten el protocolo, incluidos Google Chrome, Firefox, Microsoft Edge, Internet Explorer, Safari y Opera.

A diferencia de HTTP, WebSocket proporciona comunicación full-duplex. Además, WebSocket permite flujos de mensajes sobre TCP. TCP solo se ocupa de flujos de bytes sin un concepto inherente de un mensaje. Antes de WebSocket, la comunicación de full-duplex del puerto 80 se podía lograr mediante los canales Comet; sin embargo, la implementación de Comet no es trivial y, debido al protocolo de enlace TCP y la sobrecarga del encabezado HTTP, es ineficaz para mensajes pequeños. El protocolo WebSocket tiene como objetivo resolver estos problemas sin comprometer los supuestos de seguridad de la web.

La especificación del protocolo WebSocket definews(WebSocket) ywss(WebSocket Secure) como dos nuevos esquemas de identificador uniforme de recursos(URI)que se utilizan para conexiones cifradas y no cifradas, respectivamente. Aparte del nombre del esquema y el fragmento(es decir, no es compatible), el resto de los componentes de URI se definen para utilizar la sintaxis genérica de URI.

Primero se hizo referencia a WebSocket como TCPConnection en la especificación HTML5, como un marcador de posición para una API de socket basada en TCP. En junio de 2008, Michael Carter dirigió una serie de debates que dieron como resultado la primera versión del protocolo conocido como WebSocket.

El nombre "WebSocket" fue acuñado por Ian Hickson y Michael Carter poco después a través de la colaboración en la sala de chat #whatwg IRC, y posteriormente fue escrito por Ian Hickson para su inclusión en la especificación HTML5. En diciembre de 2009, Google Chrome 4 fue el primer navegador en ofrecer soporte completo para el estándar, con WebSocket habilitado de forma predeterminada. El desarrollo del protocolo WebSocket se trasladó posteriormente del grupo W3C y WHATWG al IETF en febrero de 2010, y fue escrito para dos revisiones bajo Ian Hickson.

Después de que el protocolo se envió y habilitó de forma predeterminada en varios navegadores, el RFC 6455 se finalizó bajo Ian Fette en diciembre de 2011.

RFC 7692 introdujo la extensión de compresión a WebSocket utilizando el algoritmo DEFLATE por mensaje.

**Protocolo Handshake**

Para establecer una conexión WebSocket, el cliente envía una solicitud de handshake o solicitud de protocolo de enlace de WebSocket, para el cual el servidor envía una respuesta de handshake, tal se muestra en el siguiente ejemplo:

Solicitud del cliente:

```
GET /chat HTTP/1.1
Host: server.example.com
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Key: x3JJHMbDL1EzLkh9GBhXDw==
Sec-WebSocket-Protocol: chat, superchat
Sec-Websocket-Version: 13
Origin: http://example.com
```

Respuesta del servidor:

```
HTTP/1.1 101 Switching Protocols
Upgrade: websocket
Connection: Upgrade
Sec-WebSocket-Accept: HSmrc0sMlYUkAGmm5OPpG2HAGWk=
Sec-WebSocket-Protocol: chat
```

El handshake comienza con una solicitud/respuesta HTTP, permitiendo al servidor manejar conexiones HTTP y conexiones WebSocket en un mismo puerto. Una vez establecida la conexión, la comunicación cambia a un protocolo binario bidireccional que no se ajusta al protocolo HTTP.

Además de los encabezados Upgrade, el cliente envía un encabezado Sec-WebSocket-key que contiene bytes aleatorios codificados en base64 y el servidor responde con un hash de la clave en el encabezado Sec-WebSocket-Accept. Esto es así para poder evitar que un proxy de almacenamiento caché envíe de vuelta una conversación anterior de WebSocket, y no proporciona ninguna privacidad, integridad ni autenticación. La función hash agrega la cadena fija (un UUID) al valor del encabezado, aplica la función hash SHA-1 y codifica el resultado usando base64.

Una vez que establecida la conexión, el cliente y el servidor pueden enviar datos de WebSocket o marcos de texto de un lado al otro en modo full-duplex. Los datos están mínimamente enmarcados, con un encabezado seguido de la carga útil.

Las transmisiones de WebSocket se describen como "mensajes", donde un solo mensaje se puede dividir opcionalmente en varios marcos de datos. Esto hace posible el envío de mensajes donde los datos iniciales están disponibles pero se desconoce la longitud completa del mensaje (envía una trama de datos tras otra hasta que se alcanza el final y se agrega el bit FIN). Con extensiones al protocolo, esto también se puede usar para multiplexar varios flujos simultáneamente (por ejemplo, para evitar monopolizar el uso de un conector para una sola carga útil grande).

**Consideraciones de Seguridad**

A diferencia de las solicitudes HTTP habituales entre dominios, las solicitudes de WebSocket no están restringidas por la política del mismo origen. Por lo tanto, los servidores de WebSocket deben validar el encabezado "Origin" contra los orígenes esperados durante el establecimiento de la conexión, para evitar ataques de secuestro de WebSocket entre sitios (similares a la falsificación de solicitudes entre sitios), que podrían ser posibles cuando la conexión se autentica con cookies o autenticación HTTP. Es mejor utilizar tokens o mecanismos de protección similares para autenticar la conexión WebSocket cuando se transfieren datos confidenciales (privados) a través de WebSocket. Un ejemplo vivo de vulnerabilidad se vio en 2020 en la forma de Cable Haunt. 

Cable Haunt es el nombre en clave asignado para representar dos vulnerabilidades distintas que afectan a muchos de los módems de cable en uso en todo el mundo en 2020. Estas vulnerabilidades permiten a un atacante obtener acceso externo a un módem de cable y realizar cualquier número de actividades destinadas a modificar el funcionamiento o monitorear los datos que pasan a través de un cable módem.

El problema radica en el sistema Broadcom en un chip, que se usa en muchos módems de cable, específicamente con el software que ejecuta el analizador de espectro, que protege contra las sobrecargas de energía en la señal del cable. Expone una interfaz WebSockets no segura a la que Cable Haunt puede acceder usando JavaScript ejecutado en el navegador de la víctima.

Los módems afectados por Cable Haunt brindan a los atacantes remotos un control total (nivel de kernel) sobre el cable módem, permitiéndoles potencialmente:
- Modificar o instalar nuevo firmware en el módem
- Cambiar el servidor DNS del módem para redirigir el tráfico saliente
- Incluir el módem en un ataque distribuido de denegación de servicio(DDoS)
- Modificar la dirección MAC o el número de serie del módem
- Deshabilitar las funciones de parcheo y actualización

**Cruce de Proxy**

Las implementaciones de cliente del protocolo WebSocket intentan detectar si el agente de usuario está configurado para usar un proxy cuando se conecta al puerto y host de destino y, si lo está, usa el método HTTP CONNECT para configurar un túnel persistente.

Si bien el protocolo WebSocket en sí mismo desconoce los servidores proxy y los firewalls, presenta un protocolo de enlace compatible con HTTP, lo que permite que los servidores HTTP compartan sus puertos HTTP y HTTPS predeterminados (80 y 443, respectivamente) con una puerta de enlace o servidor WebSocket. El protocolo WebSocket define un prefijo ws: // y wss: // para indicar una conexión WebSocket y WebSocket Secure, respectivamente. Ambos esquemas utilizan un mecanismo de actualización HTTP para actualizar al protocolo WebSocket. Algunos servidores proxy son transparentes y funcionan bien con WebSocket; otros evitarán que WebSocket funcione correctamente, provocando que la conexión falle. En algunos casos, es posible que se requiera una configuración adicional del servidor proxy y que ciertos servidores proxy deban actualizarse para admitir WebSocket.

Si el tráfico de WebSocket no cifrado fluye a través de un servidor proxy explícito o transparente sin compatibilidad con WebSockets, es probable que la conexión falle.

Si se utiliza una conexión WebSocket cifrada, entonces el uso de TLS(Transport Layer Security) en la conexión WebSocket Secure garantiza que se emite un comando HTTP CONNECT cuando el navegador está configurado para usar un servidor proxy explícito. Esto configura un túnel, que proporciona comunicación TCP de extremo a extremo de bajo nivel a través del proxy HTTP, entre el cliente WebSocket Secure y el servidor WebSocket. En el caso de los servidores proxy transparentes, el navegador no conoce el servidor proxy, por lo que no se envía HTTP CONNECT. Sin embargo, dado que el tráfico por cable está cifrado, los servidores proxy transparentes intermedios pueden simplemente permitir el paso del tráfico cifrado, por lo que hay muchas más posibilidades de que la conexión WebSocket tenga éxito si se utiliza WebSocket Secure. El uso de cifrado no está exento de costos de recursos, pero a menudo proporciona la tasa de éxito más alta, ya que viajaría a través de un túnel seguro.

**WebSockets en Python**

Principalmente debemos instalar el módulo websockets, ingresamos al símbolo del sistema e introducimos el siguiente comando: 

pip install websockets

**Ejemplo de uso de WebSockets:**

Servidor en WebSockets

Lee el nombre del cliente, envía un saludo y se cierra la conexión.

```
   import asyncio
   import websockets

   async def hello(websocket, path):
      name = await websocket.recv()
      print(f"< { name }")

      greeting = f"Hello {name}!"

      await websocket.send(greeting)
      print(f"> {greeting}")

   start_server = websockets.serve(hello, "localhost", 8765)

   asyncio.get_event_loop().run_until_complete(start_server)
   asyncio.get_event_loop().run_forever()
```


En el lado del servidor, websockets ejecuta la rutina de saludo del controlador una vez para cada conexión de WebSocket. Cierra la conexión cuando regresa la rutina del controlador.

Cliente en WebSockets

```
   import asyncio
   import websockets

   async def hello():
      uri = "ws://localhost:8765"
      async with websockets.connect(uri) as websocket:
         name = input("What's your name? ")

         await websocket.send(name)
         print(f"> {name}")

         greeting = await websocket.recv()
         print(f"< {greeting}")
   asyncio.get_event_loop().run_until_complete(hello())
```



## 3. Requerimientos de instalación.
   - ##### sudo apt install python3-pip
   - ##### pip install websockets
   - ##### pip install asyncio
   - ##### pip install json
   - ##### pip install logging

## 4. Cómo ejecutar los componentes de cada servidor.
   - Ejecutamos el servidor desde una terminal de comandos: **python servidor.py**

## 5. Cómo ejecutar el/los clientes.
   - Ejecutamos el cliente (despues del servidor) desde otra terminal de comandos: **python cliente.py**

## 6. Documentación de un API de servicios ofrecidos por el Servidor.
   - Desde la terminal donde se accede al cliente elegimos una opcion entre 0 a 7:   
        * Opcion 1: Consultar Hospitales.
            * Simplemente seleccionamos la opcion y visualizamos los hospitales disponibles con sus id correspondientes.
        * Opcion 2: Consultar Camas de un Hospital.
            * Seleccionamos la opcion y luego colocamos el id del hospital el cual queremos visualizar sus camas.
        * Opcion 3: Consultar todos los Hospitales y sus Camas.
            * Simplemente seleccionamos la opcion y visualizamos los hospitales disponibles con sus id correspondientes y sus respectivas camas.
        * Opcion 4: Agregar Cama.
            * Seleccionamos la opcion y luego colocamos el id del hospital el cual queremos agregarle una nueva cama que estara libre inicialmente.
        * Opcion 5: Eliminar Cama.
            * Seleccionamos la opcion, luego colocamos el id del hospital y por ultimo el id de la cama el cual queremos eliminar, siempre y cuando los id del hospital y de la cama existan y que la cama este libre.
        * Opcion 6: Ocupar Cama.
            * Seleccionamos la opcion, luego colocamos el id del hospital y por ultimo el id de la cama el cual queremos ocupar, siempre y cuando los id del hospital y de la cama existan y que la cama este desocupada.
        * Opcion 7: Desocupar Cama.
            * Seleccionamos la opcion, luego colocamos el id del hospital y por ultimo el id de la cama el cual queremos eliminar, siempre y cuando los id del hospital y de la cama existan y que la cama este ocupada.
        * Opcion 0: Salir del Programa.
            * Simplemente seleccionamos la opcion y automaticamente el cliente se desconecta del servidor.
