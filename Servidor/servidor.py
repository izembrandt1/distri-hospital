import websockets
import asyncio
import json
import logging

#configuracion del logging para el servidor

logging.basicConfig(filename='solicitud_cliente.log',
                    format='%(asctime)s;%(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S',
                    filemode='w',
                    level=logging.INFO
                    )

informaciones={
    0:"OK",
    2:"No existe la cama",
    4:"No existe el hospital",
    6:"La cama esta ocupada",
    8:"La cama esta desocupada",
    10:"El id recibido no es numerico",
    -1:"La operacion termino inesperadamente"
}

hospitales={
    1:{"nombre": "Clinicas", "camas":[],"cantidad":0},
    2:{"nombre": "IPS", "camas":[],"cantidad":0},
    3:{"nombre": "La Costa", "camas":[],"cantidad":0}
}

#Funcion para comprobar si un hospital existe en el diccionario
def hospital_existe(hospital_id):         
    valBool = hospital_id in hospitales
    return valBool

def ver_camas(hospital_id):
    #Se verifica que el id recibido sea numerico 
    if hospital_id.isnumeric():
        h_id=int(hospital_id)
        if hospital_existe(h_id):
            return 0       # se envia 0 para confirmar la operacion
        else:
            return 4   #El id recibido no corresponde a ningun hospital
    else:
        return 10 #El dato recibido no es numerico

def crear_cama(hospital_id):
    #Se verifica que el id recibido sea numerico 
    if hospital_id.isnumeric():
        h_id=int(hospital_id)
        if hospital_existe(h_id):
            cantidad = hospitales[h_id]['cantidad']
            hospitales[h_id]['camas'].append({'id':cantidad + 1,'ocupado':False})
            hospitales[h_id]['cantidad'] = cantidad + 1
            return 0  #enviamos 0 para informar que la operacion fue un exito
        else:
            return 4  #respuesta para saber que el hospital indicado no existe        
    else:
        return 8 #El dato recibido no es numerico
    
def eliminar_cama(hospital_id,cama_id):
    #Se verifica que el id del hospital recibido sea numerico 
    if hospital_id.isnumeric():
        h_id=int(hospital_id)
        if hospital_existe(h_id):
            #Se verifica que el id de la cama recibido sea numerico
            if cama_id.isnumeric():
                c_id = int(cama_id)
                camas = hospitales[h_id]['camas']
                for cama in camas :
                    if cama['id'] == c_id:
                        if cama['ocupado']:
                            return 6       #si la cama esta ocupada no se puede eliminar, retorna 6
                        else:
                            camas.remove(cama)
                            return 0       #entregamos 0 para confirmar la operacion
                return 2   # devolvemos 2 cuando la cama que queriamos eliminar no existe en el hospital
            else:
                return 10  #El dato recibido no es numerico
        else:
            return 4   #respuesta igual a 4 para indicar que un hospital con el id dado no existe
    else:
        return 10    #El dato recibido no es numerico

def ocupar_cama(hospital_id,cama_id):
    #Se verifica que el id de hospital recibido sea numerico 
    if hospital_id.isnumeric():
        h_id=int(hospital_id)
        if hospital_existe(h_id):
            #Se verifica que el id de cama recibido sea numerico
            if cama_id.isnumeric():
                c_id = int(cama_id)
                camas = hospitales[h_id]['camas']
                for cama in camas :
                    #Si encuentra la cama
                    if cama['id'] == c_id:
                        if not cama['ocupado']:
                            cama['ocupado'] = True
                            return 0   #Operacion realizada con exito
                        else:
                            return 6   #retornamos 6 para indicar que la cama ya estaba ocupada
                return 2  #Fallo en la operacion, cama no encontrada
            else:
                return 10 #El dato recibido no es numerico
        else:
            return 4  #El hospital dado no existe
    else:
        return 10 #El dato recibido no es numerico

def desocupar_cama(hospital_id,cama_id):
    #Se verifica que el id del hospital recibido sea numerico 
    if hospital_id.isnumeric():
        h_id=int(hospital_id)
        if hospital_existe(h_id):
            #Se verifica que el id de la cama recibido sea numerico
            if cama_id.isnumeric():
                c_id = int(cama_id)
                camas = hospitales[h_id]['camas']
                for cama in camas :
                    #Si encuentra la cama
                    if cama['id'] == c_id:
                        if cama['ocupado']:
                            cama['ocupado'] = False
                            return 0   #Operacion realizada con exito
                        else:
                            return 8   #retornamos 8 para indicar que la cama ya estaba desocupada
                return 2  #Fallo en la operacion, cama no encontrada
            else:
                return 10 #El dato recibido no es numerico
        else:
            return 4  #El hospital dado no existe
    else:
        return 10 #El dato recibido no es numerico


#Empieza a ejecutarse el codigo
PORT = 7890
print("Servidor escuchando en el puerto: " + str(PORT))

async def echo(websocket, path):
    print("Un cliente se ha conectado")
    try:
        #EL ASYNC SE EJECUTA POR CADA MENSAJE
        async for mensaje_recibido in websocket:            
            #se decodifica el mensaje recibido
            mensaje = json.loads(mensaje_recibido)
            #se almacena en el log
            logging.info(mensaje)
            hospital_id = mensaje.get("hospital_id")
            cama_id = mensaje.get("cama_id")
            operacion = mensaje.get("operacion")
            #Se realiza la operacion y recupera el estado final de la operacion
            if operacion == "ver_hospitales" or operacion == "ver_hospitales_con_camas":
                estado = 0
            elif operacion == "ver_camas":
                estado = ver_camas(hospital_id)
            elif operacion == "crear_cama":
                estado = crear_cama(hospital_id)
            elif operacion == "eliminar_cama":
                estado = eliminar_cama(hospital_id,cama_id)
            elif operacion == "ocupar_cama":
                estado = ocupar_cama(hospital_id,cama_id)
            elif operacion == "desocupar_cama":
                estado = desocupar_cama(hospital_id,cama_id)
            else:
                estado = "-1"
            #Se prepara la respuesta para el cliente
            respuesta = json.dumps({"estado":estado,"informacion":informaciones[estado],"operacion":operacion,"hospitales":hospitales,"hospital_id":hospital_id,"cama_id":cama_id})
            respuesta_enviada = respuesta.encode()
            #Se envia al cliente la respuesta procesada
            await websocket.send(respuesta_enviada)

    except websockets.exceptions.ConnectionClosed as e:
        print("Un cliente se ha desconectado")

start_server = websockets.serve(echo, "localhost", PORT)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

